<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'webdemo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$Xp+yyaGw4nF=V_[%H/m(7%SyX:1IN-Q=+rQJ&9LVIm_4DA]Z%p8l6-:TYSQ|J{4' );
define( 'SECURE_AUTH_KEY',  'o4@{r<E#bfEP&=rGcS7$2^P?t1a)b7me/h=Md8!e;U+o@(4%TT@<5b!_[i&5*n6+' );
define( 'LOGGED_IN_KEY',    'dfHXjNn{;:UWSBW!hu%W .AL3rXEw!?q+lR6yMY*:W`siI8O8h<;B(DVOx>Pi@-L' );
define( 'NONCE_KEY',        '2K.RuEvIHzPjyX?Q 5O5VzMDx,6A^yK,X8,]e8Vd+RL:I`;VV+x#Le#CImrp03*(' );
define( 'AUTH_SALT',        'k5E./eChRwhL5och<0=(QLg ^`Z0`k-MSUVvS%X.8=vr$.Q@xq%}sR8AR7PeN;4,' );
define( 'SECURE_AUTH_SALT', 'Njt:x;?vw2z%r:=R174j-zFp]g)ByL;$c9rWKzV|m?S;e/*CZ=r~SyiUp)x@,V|;' );
define( 'LOGGED_IN_SALT',   '1@3(nS:Aqoe/@5Gb|xnb%mld(a]WTy##Ms=~ OdTrxP})vXk-SXxsb)g];ULv xZ' );
define( 'NONCE_SALT',       'g@6_a9t3?hUpVP[W0J8%V4&nN-Ev!y3Hp(j.o_2:DTq!vQui{j^R{% 1:%c3wV<l' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
